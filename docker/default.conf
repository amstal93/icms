server {

    root /var/www/icms/public;
    index index.html index.php;
    server_name localhost;

    access_log /var/log/nginx/icms-access.log;
    error_log  /var/log/nginx/icms-error.log error;

    charset utf-8;

    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }
    location ~ \.css {
        add_header  Content-Type    text/css;
    }
    location ~ \.js {
        add_header  Content-Type    application/x-javascript;
    }

    location = /favicon.ico { log_not_found off; access_log off; }
    location = /robots.txt  { access_log off; log_not_found off; }

    error_page 404 /index.php;

    # PHP Settings
    location ~ \.php$ {
        try_files $uri =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass php:9000;
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param PATH_INFO $fastcgi_path_info;
    }

    # Deny .htaccess file access
    location ~ /\.ht {
        deny all;
    }
}
