<?php
namespace Nixhatter\ICMS\admin\controller;

/**
 * Site Controller
 * For use on website related admin pages.
 * Settings
 * Template
 *
 * @package ICMS
 * @author Dillon Aykac
 */

defined('_ICMS') or die;

use Nixhatter\ICMS\model;

/*
|--------------------------------------------------------------------------
| Site Controller
|--------------------------------------------------------------------------
|

*/
class siteController extends Controller
{
    public $model;
    public $user_id;
    public $fileName;
    public $user;
    public $content;
    public $template;
    public $pages;
    private $settings;

    public function getName()
    {
        return 'site';
    }

    public function __construct(model\SiteModel $model)
    {
        $this->model = $model;
        $this->model->posts = $model->posts;
        $this->settings = $model->container['settings'];
        $this->pages = "../pages/";
    }

    public function settings()
    {
        if (isset($_POST['submit'])) {
            $siteName = !empty($_POST['sitename']) ? $this->postValidation($_POST['sitename']) : null;
            $siteDesc   = !empty($_POST['sitedesc']) ? $this->postValidation($_POST['sitedesc']): null;
            $siteURL    = !empty($_POST['url']) ? $this->dotslashValidation($_POST['url']) : null;
            $siteEmail  = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
            $siteTemplate = !empty($_POST['template']) ?  $this->strictValidation($_POST['template']) : null;
            $dbhost     = !empty($_POST['dbhost'])  ?  $this->dotslashValidation($_POST['dbhost']) : null;
            $dbname     = !empty($_POST['dbname']) ? $this->strictValidation($_POST['dbname']) : null;
            $dbuser     = !empty($_POST['dbuser']) ? $this->strictValidation($_POST['dbuser']) : null;
            $dbport     = filter_input(INPUT_POST, 'dbport', FILTER_SANITIZE_NUMBER_INT);
            $emailHost  = !empty($_POST['emailHost']) ? $this->dotslashValidation($_POST['emailHost']) : null;
            $emailPort  = !empty($_POST['emailPort']) ? $this->postValidation($_POST['emailPort']): null;
            $emailUser  = !empty($_POST['emailUser']) ?$this->postValidation($_POST['emailUser']): null;
            $mailchimpapi = !empty($_POST['mailchimpapi']) ? $this->postValidation($_POST['mailchimpapi']): null;
            $mailchimplistid = !empty($_POST['mailchimplistid']) ? $this->postValidation($_POST['mailchimplistid']): null;
            $analytics  = filter_input(INPUT_POST, 'analytics', FILTER_SANITIZE_SPECIAL_CHARS);
            $mailerType = filter_input(INPUT_POST, 'mailerType', FILTER_SANITIZE_SPECIAL_CHARS);
            $emailAuth  = "";

            if ($mailerType === "oauth") {
                $emailAuth = "XOAUTH2";
                $emailClientID      = filter_input(INPUT_POST, 'emailClientID');
                $emailClientSecret  = filter_input(INPUT_POST, 'emailClientSecret');
                $this->model->hasConfigChanged("email", "clientid", $emailClientID);
                $this->model->hasConfigChanged("email", "clientsecret", $emailClientSecret);
            } elseif ($mailerType === "basic") {
                $emailAuth      = "BASIC";
                $emailPass      = filter_input(INPUT_POST, 'emailPassword');
                $this->model->hasConfigChanged("email", "pass", $emailPass);
            }

            if ($_POST['dbpass'] != "unchanged") {
                $dbpass = filter_input(INPUT_POST, 'dbpass');
                $config = "database.password";
                $this->model->editConfig($config, $dbpass);
            } else {
                $dbpass = $this->settings->production->database->password;
            }

            try {
                $dbTestConnection = new \PDO('mysql:host='.$dbhost.';port='.$dbport.';dbname='.$dbname, $dbuser, $dbpass);
                if ($dbTestConnection) {
                    $response = array('result' => 'success', 'message' => 'Updated Settings');
                }
            } catch (\PDOException $e) {
                $response = array('result' => "fail", 'message' => "Database connection failed ". $e);
            }
            $this->model->hasConfigChanged("site", "name", $siteName);
            $this->model->hasConfigChanged("site", "description", $siteDesc);
            $this->model->hasConfigChanged("site", "url", $siteURL);
            $this->model->hasConfigChanged("site", "email", $siteEmail);
            $this->model->hasConfigChanged("site", "template", $siteTemplate);
            $this->model->hasConfigChanged("database", "host", $dbhost);
            $this->model->hasConfigChanged("database", "name", $dbname);
            $this->model->hasConfigChanged("database", "port", $dbport);
            $this->model->hasConfigChanged("database", "user", $dbuser);
            $this->model->hasConfigChanged("database", "password", $dbpass);
            $this->model->hasConfigChanged("email", "auth", $emailAuth);
            $this->model->hasConfigChanged("email", "host", $emailHost);
            $this->model->hasConfigChanged("email", "port", $emailPort);
            $this->model->hasConfigChanged("email", "user", $emailUser);

            $this->model->hasConfigChanged("site", "analytics", $analytics);
            $this->model->hasConfigChanged("addons", "mailchimpapi", $mailchimpapi);
            $this->model->hasConfigChanged("addons", "mailchimplistid", $mailchimplistid);

            // Minify the CSS file
            $this->minifyCSS();
            exit(json_encode($response));
        }
    }

    public function template()
    {
        $this->template = $this->model->getTemplatePath($this->settings->production->site->template);
        if (!empty($_POST['file'])) {
            $file = $this->fileValidation($_POST['file']);
        } else {
            $file = $this->template.'index.php';
        }
        if (isset($_POST['submit'])) {
            // TODO: Sanitize TemplateContent
            if ($this->model->editTemplate($file, $_POST['templateContent'])) {
                $response = array('result' => 'success', 'message' => 'Updated Template');
                exit(json_encode($response));
            }
        }

        $this->content = file_get_contents($file);
        $this->fileName = $file;
    }

    // Depreciated
    public function scan()
    {
        if (isset($_GET['scan'])) {
            //Scan Root folder
            $files = scandir(".");
            $pages = scandir("../pages/");
            //Scan Admin folder
            $admin = scandir(__DIR__);


            $lines = file('../core/configuration.php');
            $result = '';

            foreach ($admin as $key=>&$value) {
                if (strlen($value) < 3) {
                    unset($admin[$key]);
                }
            }

            foreach ($lines as $line) {
                if (substr($line, 0, 9) == 'site.core') {
                    $result .= "site.core = [".implode(", ", $files)."]\n";
                } elseif (substr($line, 0, 10) == 'site.pages') {
                    $result .= "site.pages = [".implode(", ", $pages)."]\n";
                } elseif (substr($line, 0, 10) == 'site.admin') {
                    $result .= "site.admin = [".implode(", ", $admin)."]\n";
                } else {
                    $result .= $line;
                }
            }
            file_put_contents('../core/configuration.php', $result);
            $response = array('result' => 'success', 'message' => 'Scanned Successfully');
            exit(json_encode($response));
        }
    }

    //TODO: A security orientated dashboard
    public function controlcenter()
    {
    }

    // For getting Googles oauth token
    public function oauth()
    {
        //header("Location: /get_oauth_token.php");
        $redirectUri = $this->settings->production->site->url."/admin/site/oauth";
        //$redirectUri = "http://".$this->settings->production->site->url."/get_oauth_token.php";

        //These details obtained are by setting up app in Google developer console.
        $clientId = $this->settings->production->email->clientid;
        $clientSecret = $this->settings->production->email->clientsecret;
        require 'get_oauth_token.php';
        //header('Location: /admin/site/settings');
        exit();
    }

    // Defines the CSS Class for an active variable
    public function isActive($variable)
    {
        if (!empty($variable)) {
            return "active";
        } else {
            return "";
        }
    }

    /**
     * Automatic Sitemap Generation
     * It uses a crawler that follows all links and
     * then generates the sitemap in the root directory.
     */
    public function sitemap()
    {
        require("../core/admin/controller/Sitemap.php");
        new \Sitemap($this->settings->production->site->url, "daily");
    }

    /**
     * Combine all required CSS files and minify them.
     */
    public function minifyCSS()
    {
        $mincss = "";

        // CSS Minifier => https://gist.github.com/tovic/d7b310dea3b33e4732c0
        function minify_css($input)
        {
            if (trim($input) === "") {
                return $input;
            }
            // Force white-space(s) in `calc()`
            if (strpos($input, 'calc(') !== false) {
                $input = preg_replace_callback('#(?<=[\s:])calc\(\s*(.*?)\s*\)#', function ($matches) {
                    return 'calc(' . preg_replace('#\s+#', "\x1A", $matches[1]) . ')';
                }, $input);
            }
            return preg_replace(
                array(
                    // Remove comment(s)
                    '#("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\')|\/\*(?!\!)(?>.*?\*\/)|^\s*|\s*$#s',
                    // Remove unused white-space(s)
                    '#("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\'|\/\*(?>.*?\*\/))|\s*+;\s*+(})\s*+|\s*+([*$~^|]?+=|[{};,>~+]|\s*+-(?![0-9\.])|!important\b)\s*+|([[(:])\s++|\s++([])])|\s++(:)\s*+(?!(?>[^{}"\']++|"(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\')*+{)|^\s++|\s++\z|(\s)\s+#si',
                    // Replace `0(cm|em|ex|in|mm|pc|pt|px|vh|vw|%)` with `0`
                    '#(?<=[\s:])(0)(cm|em|ex|in|mm|pc|pt|px|vh|vw|%)#si',
                    // Replace `:0 0 0 0` with `:0`
                    '#:(0\s+0|0\s+0\s+0\s+0)(?=[;\}]|\!important)#i',
                    // Replace `background-position:0` with `background-position:0 0`
                    '#(background-position):0(?=[;\}])#si',
                    // Replace `0.6` with `.6`, but only when preceded by a white-space or `=`, `:`, `,`, `(`, `-`
                    '#(?<=[\s=:,\(\-]|&\#32;)0+\.(\d+)#s',
                    // Minify string value
                    '#(\/\*(?>.*?\*\/))|(?<!content\:)([\'"])([a-z_][-\w]*?)\2(?=[\s\{\}\];,])#si',
                    '#(\/\*(?>.*?\*\/))|(\burl\()([\'"])([^\s]+?)\3(\))#si',
                    // Minify HEX color code
                    '#(?<=[\s=:,\(]\#)([a-f0-6]+)\1([a-f0-6]+)\2([a-f0-6]+)\3#i',
                    // Replace `(border|outline):none` with `(border|outline):0`
                    '#(?<=[\{;])(border|outline):none(?=[;\}\!])#',
                    // Remove empty selector(s)
                    '#(\/\*(?>.*?\*\/))|(^|[\{\}])(?:[^\s\{\}]+)\{\}#s',
                    '#\x1A#'
                ),
                array(
                    '$1',
                    '$1$2$3$4$5$6$7',
                    '$1',
                    ':0',
                    '$1:0 0',
                    '.$1',
                    '$1$3',
                    '$1$2$4$5',
                    '$1$2$3',
                    '$1:0',
                    '$1$2',
                    ' '
                ),
                $input
            );
        }

        $min_stylesheet = "templates/".$this->settings->production->site->template."/css/style.min.css";
        if (file_exists($min_stylesheet)) {
            unlink($min_stylesheet);
        }

        foreach (glob("templates/".$this->settings->production->site->template."/css/*.css") as $file) {
            $stylesheet = file_get_contents($file);
            $stylesheet = minify_css($stylesheet);
            $mincss .= $stylesheet;
        }

        if (file_put_contents("templates/".$this->settings->production->site->template."/css/style.min.css", $mincss)) {
            $response = array('result' => 'success', 'message' => 'style.min.css generated successfully.');
        } else {
            $response = array('result' => "failed", 'message' => 'style.min.css could not be generated');
        }
        echo(json_encode($response));
        exit();
    }

    // TODO: Find a working expression to minify JS
    public function minifyJS()
    {
        $minjs = "";

        // JavaScript Minifier -> https://gist.github.com/tovic/d7b310dea3b33e4732c0
        function minify_js($input)
        {
            return $input;
        }

        unlink("templates/".$this->settings->production->site->template."/js/main.min.js");
        foreach (glob("templates/".$this->settings->production->site->template."/js/*.js") as $file) {
            $javascript = file_get_contents($file);
            $javascript = minify_js($javascript);
            $minjs .= $javascript;
        }

        if (file_put_contents("templates/".$this->settings->production->site->template."/js/main.min.js", $minjs)) {
            $response = array('result' => 'success', 'message' => 'main.min.js generated successfully.');
        } else {
            $response = array('result' => "failed", 'message' => 'main.min.js could not be generated');
        }
        echo(json_encode($response));
        exit();
    }

    /**
     * Send a test email to the site admin to see if it works.
     */
    public function testEmail()
    {
        $mail = $this->model->mail($this->settings->production->site->email, $this->settings->production->site->name, "Test Email", "If you've received this email then everything's working properly.");
        if ($mail) {
            $response = array('result' => 'success', 'message' => 'Email has been sent!');
        } else {
            $response = array('result' => "failed", 'message' => 'Could not send email.');
        }
        echo(json_encode($response));
        exit();
    }

    public function update()
    {
        require('update.php');
        start();
    }
}
